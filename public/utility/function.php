<?php
    function sort_by ($items, $key) {
        usort($items, function($a, $b) use ($key) {
            if ($a[$key] == $b[$key]) return 0;
            return ($a[$key] < $b[$key]) ? -1 : 1;
        });

        return $items;
    }
?>
