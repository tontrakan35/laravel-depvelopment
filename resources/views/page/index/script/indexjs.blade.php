@section('scirpt')
<script type="text/javascript">
    const districtInfo = {{ Illuminate\Support\Js::from($districtInfo) }};
    $( document ).ready(function() {
        const myFunc = {
            init: function() {
                const store = localStorage.getItem('infomation');
                myFunc.overEvent();
                $('[data-form="insert"]').trigger('click');
                if(store) myFunc.setResults(JSON.parse(store));
            },
            overEvent: function() {
                $('select[data-item="cityCode"]').on('change', function($event) {
                    const districts = districtInfo.filter((item) => item.city_id == $event.target.value);
                    const option$ = $('option');
                    const option = districts.map((item) => new Option(item.districtTH, item.id));
                    $('[data-item="districtCode"]').html(new Option('กรุณาเลือก', '')).append(option);
                })

                $('[data-form]').on('click', function() {
                    const elm$ = $(this);
                    $('[data-form]').removeClass('tabActive');
                    if(elm$.length) {
                        switch(elm$.data('form')) {
                            case 'search':
                                $('button[data-event="search"]').show();
                                $('button[data-event="save"]').hide();
                            break;
                            case 'insert':
                                $('button[data-event="search"]').hide();
                                $('button[data-event="save"]').show();
                            break;
                        }
                        elm$.addClass('tabActive');
                    }
                })

                $('[data-event="save"]').on('click', myFunc.saveInfomation)

                $('[data-event="search"]').on('click', myFunc.searchInfomation)

                $('[data-event="clear"]').on('click', myFunc.clearInfomation)
            },
            validator: function() {

                const item$ = $('[data-item]');
                let valid = true;
                let param = {};

                $.each(item$, function(i, v) {
                    const elm$ = $(v);
                    const parent$ = elm$.closest('div');
                    if(parent$.find('span.text-danger')?.length && !elm$.val()) {
                        valid = false;
                        return;
                    }

                    if(elm$.is('select')) {
                        param[elm$.data('item').replace('Code', 'Name')] = elm$.find('option:selected').text();
                    }

                    param[elm$.data('item')] = elm$.val();
                })

                return [valid, param];

            },
            saveInfomation: function() {
                let [valid, param] = myFunc.validator();
                let infomation = [];
                const store = localStorage.getItem('infomation');
                if(!valid) {
                    myFunc.popup('error', 'กรุณากรอกข้อมูลให้ครบ ก่อนทำรายการต่อไป');
                    return false;
                }
                if(store) infomation = JSON.parse(store);
                infomation.push(param);
                localStorage.setItem('infomation', JSON.stringify(infomation));
                myFunc.setResults(infomation);
                myFunc.popup('success');
            },
            searchInfomation: function() {
                let [valid, param] = myFunc.validator();
                let infomation = [];
                const store = localStorage.getItem('infomation');
                if(!valid) {
                    myFunc.popup('error', 'กรุณากรอกข้อมูลให้ครบ ก่อนทำรายการต่อไป');
                    return false;
                }
                if(store) infomation = JSON.parse(store);

                const data = infomation.filter((item) => {
                    // let valid = true;
                    if(param.deedNo && param.deedNo != item.deedNo) {
                        return false;
                    }

                    if(param.explorePage && param.explorePage != item.explorePage) {
                        return false;
                    }

                    if(param.cityCode && param.cityCode != item.cityCode) {
                        return false;
                    }

                    if(param.districtCode && param.districtCode != item.districtCode) {
                        return false;
                    }

                    return true;
                });
                myFunc.setResults(data);
            },
            clearInfomation: function() {
                $('[data-item]').val('');
            },
            setResults: function (item) {
                const html = `<div class="d-flex flex-wrap  border-bottom">
                                <div class="col-3 py-3 px-2">@{{deedNo}}</div>
                                <div class="col-3 py-3 px-2">@{{explorePage}}</div>
                                <div class="col-3 py-3 px-2">@{{cityName}}</div>
                                <div class="col-3 py-3 px-2">@{{districtName}}</div>
                            </div>`;
                let result = [];
                result = item.map((item) => {
                    return html.replace('@{{deedNo}}', item.deedNo)
                        .replace('@{{explorePage}}', item.explorePage)
                        .replace('@{{cityName}}', item.cityName)
                        .replace('@{{districtName}}', item.districtName);
                });

                $('[data-component="result"]').html(result);
            },
            popup: function ($mode, $text = '') {
                let model$ = $('#alertify');
                switch($mode) {
                    case 'success':
                        // console.log(model$.find('[data-bs="detail"]'));
                        model$.find('[data-bs="detail"]').html('<i class="fas fa-check-circle text-success"></i> <b>สำเร็จ</b>');
                        model$.modal('show');
                        setTimeout(() => { myFunc.popup('close') }, 1000);
                    break;
                    case 'error':
                        model$.find('[data-bs="detail"]').html($text);
                        model$.modal('show');
                        setTimeout(() => { myFunc.popup('close') }, 1000);
                    break
                    case 'close':
                        model$.modal('hide');
                    break;
                }
            }
        }

        myFunc.init();
    });
</script>
@endsection