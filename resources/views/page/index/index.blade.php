@extends('layout.index')
@section('container')
<div class="d-flex flex-wrap justify-content-between bg-white border p-4">
    <div class="fs-2"><b>Laravel Development</b></div>
    <!-- <div class="d-flex flex-wrap justify-content-between">
        <div class="align-self-center px-1 btn">login</div>
        <div class="align-self-center px-1 btn">register</div>
    </div> -->
</div>
<div class="bg-light d-flex flex-wrap justify-content-center" style="height: auto; min-height: 75vh">
    <div class="col-10 col-sm-8 col-lg-6 my-4">
        <div class="col-12 d-flex flex-wrap justify-content-center">
            <div class="col-4 text-center bg-white btn mx-2 p-3 shadow-sm fw-bold" data-form="search">ค้นหาข้อมูล</div>
            <div class="col-4 text-center bg-white btn mx-2 p-3 shadow-sm fw-bold" data-form="insert">เพิ่มข้อมูล</div>
        </div>
        <div class="col-12 bg-white shadow-sm mt-4 p-1">
            <span class="text-secondary">
                กรุณาระบุรายละเอียดค้นหา
            </span>
            <div class="row col-12 p-3">
                <div class="mb-3 col-12 col-sm-6">
                    <label class="form-label">เลขที่โฉนด <span class="text-danger">*</span></label>
                    <input type="email" class="form-control" data-item="deedNo" placeholder="กรุณากรอกข้อมูล">
                </div>
                <div class="mb-3 col-12 col-sm-6">
                    <label class="form-label">หน้าสำรวจ</label>
                    <input type="email" class="form-control" data-item="explorePage"  placeholder="กรุณากรอกข้อมูล">
                </div>
                <div class="mb-3 col-12 col-sm-6">
                    <label class="form-label">จังหวัด <span class="text-danger">*</span></label>
                    <select class="form-select" data-item="cityCode">
                        <option value="" selected>กรุณาเลือก</option>
                        @foreach ( $cityInfo as $item )
                            <option value="{{$item['id']}}">{{$item['provinceTH']}}</option>
                        @endforeach
                    </select>
                </div>
                <div class="mb-3 col-12 col-sm-6">
                    <label class="form-label">อำเภอ/เขต</label>
                    <select class="form-select" data-item="districtCode">
                        <option value="" selected>กรุณาเลือก</option>
                    </select>
                </div>
                <div class="col-12">
                    <span class="text-primary text-decoration-underline btn p-0">วิธีการค้นหาราคาประเมิน</span>
                </div>
                <div class="col-12 d-flex flex-wrap justify-content-center mt-3">
                    <button type="button" data-event="search" class="d-node btn btn-primary mx-2">ค้นหา</button>
                    <button type="button" data-event="save" class="d-node btn btn-primary mx-2">บันทึก</button>
                    <button type="button" data-event="clear" class="btn btn-danger mx-2">ล้าง</button>
                </div>
            </div>
        </div>
        <div class="col-12 bg-white shadow-sm mt-4 px-2 pb-4">
            <div class="d-flex flex-wrap  border-bottom border-3 border-info">
                <div class="col-3 py-3 px-2"><strong>เลขที่โฉนด</strong></div>
                <div class="col-3 py-3 px-2"><strong>หน้าสำรวจ</strong></div>
                <div class="col-3 py-3 px-2"><strong>จังหวัด</strong></div>
                <div class="col-3 py-3 px-2"><strong>อำเภอ / เขต</strong></div>
            </div>
            <div data-component="result">

            </div>
        </div>
    </div>
</div>
@includeIf('component.model-alert', ['name' => 'alertify'])
@endsection

@includeIf('page.index.script.indexjs')