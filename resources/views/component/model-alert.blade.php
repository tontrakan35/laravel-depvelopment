<div class="modal fade" aria-hidden="true" data-bs-backdrop="static" data-bs-keyboard="false" id="{{ $name }}" tabindex="-1">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-body text-center fs-3" data-bs="detail"></div>
        </div>
    </div>
</div>
