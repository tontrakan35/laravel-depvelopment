<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class Home extends Controller
{
    public function __construct() {

    }

    public function view() {
        $cityInfo = json_decode(file_get_contents(public_path() . "/json/cities.json"), true);
        $districtInfo = json_decode(file_get_contents(public_path() . "/json/districtInfo.json"), true);
        $cityInfo = sort_by($cityInfo, 'provinceTH');
        $districtInfo = sort_by($districtInfo, 'districtTH');
        // print_r($cityInfo);
        return view('page.index.index', ["cityInfo" => $cityInfo, "districtInfo" => $districtInfo, "pageName" => "Home"]);
    }
}
